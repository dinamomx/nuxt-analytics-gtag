const { resolve } = require('path')

module.exports = function module(moduleOptions) {
  const options = this.options['nuxt-analytics-gtag'] || moduleOptions

  this.addPlugin({
    src: resolve(__dirname, './plugins/nuxt-analytics-gtag.template.js'),
    fileName: 'nuxt-analytics-gtag.js',
    options,
    ssr: false,
  })
}
