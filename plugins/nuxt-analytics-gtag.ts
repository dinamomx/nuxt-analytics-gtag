// const options = <%= serialize(options) %>
import { Plugin } from '@nuxt/types'

const options: {
  analyticsTrackingIDs: string[]
  adwordsTrackingIDs: string[]
  primaryId: string
} = {
  analyticsTrackingIDs: [],
  adwordsTrackingIDs: [],
  primaryId: '',
}

declare module '@nuxt/types' {
  interface Context {
    $gtag(): void
  }
}

declare global {
  interface Window {
    dataLayer: any[]
    gtag: (...args: any[]) => void | Gtag.Gtag
  }
}

const nuxtAnalyticsGtag: Plugin = ({ app }, inject) => {
  /* Creamos los elementos globales o los usamos si ya existen para compatibilidad */
  let localGtag: (...args: any[]) => void = () => {}
  if (process.client) {
    const dataLayer = window.dataLayer || []
    window.dataLayer = dataLayer
    window.gtag =
      window.gtag ||
      function gtag() {
        // eslint-disable-next-line prefer-rest-params
        dataLayer.push(arguments)
      }
    localGtag = window.gtag
  }

  function vueGtag(...args: any[]) {
    localGtag(...args)
  }

  /** @type {string[]} */
  vueGtag.analyticsTrackingIDs = options.analyticsTrackingIDs
  /** @type {string[]} */
  vueGtag.adwordsTrackingIDs = options.adwordsTrackingIDs
  /**
   * Devuelve todas las id's de rastreo, adwords o analytics.
   *
   * @returns {string[]} Las id's de rastreo
   */
  vueGtag.getAllTrackingIds = function getAllTrackingIds() {
    return [...this.analyticsTrackingIDs, ...this.adwordsTrackingIDs]
  }
  /**
   * Registra el evento "generate_lead"
   *
   * @param {string} eventLabel Etiqueta para el evento
   * @param {Function} eventCallback Callback para cuando el evento se envió.
   */
  vueGtag.doReportLead = function doReportLead(
    eventLabel = `Formulario de contacto enviado. ${document.title}`,
    eventCallback = () => {}
  ) {
    this('event', 'generate_lead', {
      event_label: eventLabel,
      event_category: 'engagement',
      event_callback: eventCallback,
    })
  }
  /**
   * Registra la navegación a todas las id's de rastreo
   *
   * @param {string} path La ruta a registrar
   */
  vueGtag.doTrackPage = function doTrackPage(path: string) {
    this.getAllTrackingIds().forEach(trackingId =>
      this('config', trackingId, {
        page_path: path,
      })
    )
  }

  // Inyectamos el modulo a la instancia de vue
  inject('gtag', vueGtag)

  // Every time the route changes (fired on initialization too)
  if (app.router) {
    app.router.afterEach(to => {
      vueGtag.doTrackPage(to.path)
    })
  }
  // Only run only in production mode
  if (process.env.NODE_ENV !== 'production') return

  vueGtag('js', new Date())
  vueGtag
    .getAllTrackingIds()
    .forEach(trackingId => vueGtag('config', trackingId))

  if (process.client) {
    const d = document
    const s = d.createElement('script')
    s.async = true
    s.src = `https://www.googletagmanager.com/gtag/js?id=${options.primaryId}`
    d.head.appendChild(s)
  }
}

export default nuxtAnalyticsGtag
